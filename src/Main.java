
public class Main {

	static String KEY_PATH = "key.txt";
	static String IV = "AAAAAAAAAAAAAAAA";
	
	public static void main(String[] args) {
		
		BigEncriptaDecriptaAES aes = new BigEncriptaDecriptaAES(KEY_PATH, IV);
		String type = args[0];
		String fileName = args[1];
		String outFile = args[2];
		System.out.println("Start AES");
		switch (type) {
			case "-e":
				System.out.println("Start Encrypt: " + fileName +" - " + outFile );
				try {
					aes.encrypt(KEY_PATH, fileName,outFile);
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("End Encrypt: " + fileName +" - " + outFile );
				break;
			case "-d":
				System.out.println("Start Decrypt");
				try {
					aes.decrypt(fileName, outFile);
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("End Decrypt: " + fileName +" - " + outFile );
				break;
			default:
				System.out.println("Par�metros Inv�lidos");
				break;
		}
		System.out.println("End AES");
		
		
	}

}
