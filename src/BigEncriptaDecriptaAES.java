import java.io.File;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;


public class BigEncriptaDecriptaAES { 
	String IV; 
	static byte[] chaveencriptacaoBytes;
	static byte textoencriptado[];
	byte[] buf = new byte[1024];
	private BufferedInputStream bis;
	private static FileInputStream fileInputStream;

	
		public BigEncriptaDecriptaAES(String fileKeyPath, String IV) { 
			this.IV = IV;
		} 
		
		public void encrypt(String fileKeyPath, String fileInPath, String fileOutSendPath) throws Exception {
			String fileOutPath = fileOutSendPath;
			generateSecreteKey(fileKeyPath);
			Cipher encripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE"); 

			try{
				SecretKeySpec key = new SecretKeySpec(chaveencriptacaoBytes, "AES"); 
				encripta.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8"))); 
				
				InputStream in = new FileInputStream(fileInPath);
				OutputStream ou = new FileOutputStream(fileOutPath);
				encrypt(in,ou, encripta);
			}catch (FileNotFoundException e) {
	            System.out.println("File not found" + e);
	        }
	        catch (IOException ioe) {
	            System.out.println("Exception while reading file " + ioe);
	        }						
		/*	File[] files = new File[2];
			files[0] = new File(fileKeyPath);
			files[1] = new File(fileOutPath);*/
			//File mergedFile = new File(fileOutSendPath);
			//mergeFiles(files, mergedFile);*/
			
		} 
		
		public void encrypt(InputStream in, OutputStream out, Cipher ecipher)
		{
			try
			{
				out = new CipherOutputStream(out, ecipher);
				int numRead = 0;
				while ((numRead = in.read(buf)) >= 0)
				{
					out.write(buf, 0, numRead);
				}
				out.close();
			}
			catch (java.io.IOException e)
			{
			}
		}
		
		public void generateSecreteKey(String filePath) throws NoSuchAlgorithmException, 
				InvalidKeySpecException, IOException{
		    File keyfile = new File(filePath);
			//SecretKey key = generateKey();
		    //writeKey(key, keyfile);
		    writeKey(nextIntInRange(1,32), keyfile);
		}
		
		public SecretKey generateKey() throws NoSuchAlgorithmException {		    
		    KeyGenerator keygen = KeyGenerator.getInstance("AES");
		    return keygen.generateKey();
		 }
		
		 /** Save the specified TripleDES SecretKey to the specified file */
	    public static void writeKey(SecretKey key, File f) throws IOException,
			      NoSuchAlgorithmException, InvalidKeySpecException {
			   
				byte[] encoded = key.getEncoded();
			    // Write the raw key to the file
			    FileOutputStream out = new FileOutputStream(f);
			    chaveencriptacaoBytes = encoded;
			    out.write(encoded);
			    out.close();
	   }
		  
	    public static void writeKey(String key, File f) throws IOException{
	   
		byte[] encoded = key.getBytes();
	    // Write the raw key to the file
	    FileOutputStream out = new FileOutputStream(f);
	    chaveencriptacaoBytes = encoded;
	    out.write(encoded);
	    out.close();
	  }
		
		public byte[] readKeyBytes(String fileInPath){
	         
		        //Delimiter used in CSV file
			try {
				File file = new File(fileInPath);
				File keyFile = new File(Main.KEY_PATH);

				int sizeOfKey = 32;// 32bytes
				int sizeOfFile = (int)file.length();

		        bis = new BufferedInputStream(new FileInputStream(file));
		        BufferedInputStream bisKey = new BufferedInputStream(new FileInputStream(keyFile));

		        
		        byte[] buff=new byte[sizeOfKey];
		        for (int i=0; i < sizeOfKey; i++) {
		        	buff[i]=(byte)bisKey.read();
		        }
		        
		        /*System.out.println(sizeOfFile);
		        byte[] buffer = new byte[sizeOfFile];
		        for (int i=0; i < (sizeOfFile); i++) {
		        	buffer[i]=(byte)bis.read();
		        }*/
		       //textoencriptado = buffer;
			   System.out.println("SIZE: " + sizeOfFile);
		       //System.out.println("textoencriptado SIZE: " + buffer.length);
		       System.out.println("key SIZE: " + buff.length);
		       String v = new String( buff, Charset.forName("UTF-8") );
		       System.out.println("key: " + v);

			   return buff;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return null;  
		}
		public void decrypt(String fileInPath, String fileOutPath) throws Exception{ 

			try{
				byte[] rawkey = readKeyBytes(fileInPath);
				
				Cipher decripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE"); 
				
				SecretKeySpec key = new SecretKeySpec(rawkey, "AES"); 
				
				decripta.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8"))); 

				InputStream in = new FileInputStream(fileInPath);
				OutputStream out = new FileOutputStream(fileOutPath);
				decrypt(in,out,decripta);
			}catch (FileNotFoundException e) {
	            System.out.println("File not found" + e);
	        }
	        catch (IOException ioe) {
	            System.out.println("Exception while reading file " + ioe);
	        }
			
		} 
		
		public void decrypt(InputStream in, OutputStream out, Cipher dcipher)
		{
			try
			{
				in = new CipherInputStream(in, dcipher);
				int numRead = 0;
				while ((numRead = in.read(buf)) >= 0)
				{
					out.write(buf, 0, numRead);
				}
				out.close();
			}
			catch (java.io.IOException e)
			{
			}
		}
		
		
	public String nextIntInRange(int min, int max) {
				Random rng = new Random();
				String saida = "";
				for(int i=min;i<=max;i++){
					saida = saida.concat(""+rng.nextInt(9));
				}
				return saida;
	}
			
			
			
     public static void mergeFiles(File[] files, File mergedFile) {
				 
	            OutputStream out = null;
				try {
					out = new FileOutputStream(mergedFile);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}

				for (File f : files) {
					System.out.println("merging: " + f.getName());
					try {
						fileInputStream = new FileInputStream(f);
			            InputStream in = new FileInputStream(f);
		 
						byte[] buf = new byte[1024];
				        int len;
				        while ((len = in.read(buf)) > 0){
				                out.write(buf, 0, len);
				        }
				        in.close();
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		 
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		 
			}
			
			
	}

